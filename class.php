<?php
//EVGENI TEMNIKOV 321775272
class htmlpage{
    protected $title="";
    protected $body="";
    function __construct($title ="", $body=""){
        $this->title = $title;
        $this->body = $body;
        }
    
    public function view(){
        echo 
        "<html>
        <head>
            <title>$this->title</title>
        </head>
        <body>
            $this->body
        </body>
        </html>";
    }

}


class coloredBodyText extends htmlpage {
    protected $color = ""; #default will be black color
    protected $colors = array ('red', 'blue', 'yellow', 'green');
    public function __set($property, $value){
        if ($property == 'color'){
            if (in_array($value, $this->colors)){
                $this->color = $value;
            }
            else{
                exit('ALERT MESSAGE:   NO VALID COLOR ENTERED') ;
            }
        }
    }
    public function view(){
        echo 
        "<html>
        <head>
            <title>$this->title</title>
        </head>
        <body>
           <p style = 'color:$this->color' > $this->body</p>
        </body>
        </html>";
    }
}


class DynamicFontSize extends coloredBodyText {
    protected $fontSize = "";
    public function __set($property, $value){
        $sizes = range(10,24);
        if ($property == 'fontSize'){
            if (in_array($value, $sizes)){
                $this->fontSize = $value;
            }
            else{
                exit('ALERT MESSAGE:   NO VALID FONT SIZE, ENTER FONT SIZE IN RANGE 10 TO 24') ;
            }
        }
        if ($property == 'color') {
            parent::__set($property, $value); // Use Parent  Color Setter 
        }
        
    }

    public function view(){
        echo 
        "<html>
        <head>
            <title>$this->title</title>
        </head>
        <body>
           <p style = 'font-size:$this->fontSize ; color:$this->color' > $this->body</p>
        </body>
        </html>";
    }
}

?>
